package com.example.businessman

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class EditProductFragmentTest {

    @Test fun testDisplay() {

        launchFragmentInContainer<EditProductFragment>()

        onView(withId(R.id.edit_product_name_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.edit_product_price_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.edit_product_quantity_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.imageView4))
            .check(matches(isDisplayed()))

        onView(withId(R.id.new_product_fab))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testContent(){
        launchFragmentInContainer<EditProductFragment>()

        onView(withId(R.id.edit_product_name_tv))
            .check(matches(withText(R.string.product_name)))

        onView(withId(R.id.edit_product_quantity_tv))
            .check(matches(withText(R.string.product_quantity)))

        onView(withId(R.id.new_product_fab))
            .check(matches(isChecked()))

    }
}