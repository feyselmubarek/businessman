package com.example.businessman.EndToEndTesting

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import com.example.businessman.MainActivity
import com.example.businessman.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class AddProductE2ETest {

    @Test
    fun addProduct(){

        ActivityScenario.launch(MainActivity::class.java)

        Espresso.onView(ViewMatchers.withId(R.id.username)).perform(ViewActions.typeText("feysel"))

        Espresso.onView(ViewMatchers.withId(R.id.password_et)).perform(ViewActions.typeText("faya"))

        Espresso.onView(ViewMatchers.withId(R.id.signInButton)).perform(ViewActions.click())


        Espresso.onView(ViewMatchers.withId(R.id.product_recycler_view))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))


        Espresso.onView(ViewMatchers.withId(R.id.new_product_fab)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.create_product_name_tv))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.create_product_name_tv)).perform(ViewActions.typeText("New Product"))

        Espresso.onView(ViewMatchers.withId(R.id.create_product_price_TV)).perform(ViewActions.typeText("130"))

        Espresso.onView(ViewMatchers.withId(R.id.create_product_quantity_tv)).perform(ViewActions.typeText("12"))

        Espresso.onView(ViewMatchers.withId(R.id.pick_button)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.error_message_TV))
            .check(ViewAssertions.matches(ViewMatchers.withText("Added new Product")))

    }
}