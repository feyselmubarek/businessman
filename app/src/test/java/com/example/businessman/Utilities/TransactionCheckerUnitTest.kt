package com.example.businessman.Utilities

import org.junit.Test

class TransactionCheckerUnitTest{
    @Test
    fun testProductPrice(){
        val incorrectPassword = -0.8
        val incorrectResult = TransactionChecker.checkProductPrice(incorrectPassword)
        val correctPassword = 9.0
        val correctResult = TransactionChecker.checkProductPrice(correctPassword)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductQuantity(){
        val correctUserName = 7
        val correctResult = TransactionChecker.checkProductQuantity(correctUserName)
        val incorrectUserName = -10
        val incorrectResult = TransactionChecker.checkProductQuantity(incorrectUserName)

        assert(!incorrectResult)
        assert(correctResult)
    }
}