package com.example.businessman


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.businessman.databinding.FragmentProductTransactionBinding
import com.example.businessman.viewModel.ProductTransactionViewModel
import kotlinx.android.synthetic.main.fragment_new_product_fragment.view.*
import kotlinx.android.synthetic.main.fragment_product_transaction.view.*


/**
 * A simple [Fragment] subclass.
 *
 */
class ProductTransactionFragment : Fragment() {
    private lateinit var binding: FragmentProductTransactionBinding
    private lateinit var viewModel : ProductTransactionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_transaction, container, false)
        viewModel = ViewModelProviders.of(this).get(ProductTransactionViewModel::class.java)
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.newTransactionProgressBar = binding.root.saleProgressBar
        viewModel.newTransactionProgressBar.visibility = View.INVISIBLE

        arguments?.let{
            viewModel.productId = it.getInt("product_Id")
        }

        binding.viewModel = viewModel
        return binding.root
    }

}
