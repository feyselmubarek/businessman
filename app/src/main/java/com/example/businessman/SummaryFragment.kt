package com.example.businessman

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.androidplot.xy.*
import com.example.businessman.databinding.FragmentSummaryBinding
import com.example.businessman.viewModel.SummaryViewModel
import kotlinx.android.synthetic.main.fragment_summary.view.*

/**
 * A simple [Fragment] subclass.
 *
 */
class SummaryFragment : Fragment() {

    private lateinit var barChart: XYPlot
    lateinit var binding: FragmentSummaryBinding
    lateinit var viewModel: SummaryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater ,R.layout.fragment_summary, container, false)
        viewModel = ViewModelProviders.of(this).get(SummaryViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
        viewModel.barChart = binding.root.barChart
        setHasOptionsMenu(true)

        viewModel.sharedPreferences?.let {
            val userId = it.getInt("user_id", 0)
            println("user id: $userId")
            viewModel.getMyTransaction(userId)
        }

//        barChart = binding.root.barChart
//        var domainLabels = listOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
//
//        var series1Numbers = listOf(1, 4, 2, 8, 4, 16, 8, 32, 16, 64, 11, 12)
//        var seriesMidNumbers = listOf(1, 2, 1, 4, 2, 10, 5, 20, 6, 24, 11, 12)
//        var series2Numbers = listOf(5, 2, 10, 5, 20, 10, 40, 20, 80, 40, 11, 12)
//
//        val series1 = SimpleXYSeries(series1Numbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Profit")
//        val seriesMd = SimpleXYSeries(seriesMidNumbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Sales")
//        val series2 = SimpleXYSeries(series2Numbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Loss")
//
//        var series1Format = LineAndPointFormatter(Color.DKGRAY, Color.GREEN, null, null)
//        var seriesMidFormat = LineAndPointFormatter(Color.RED, Color.BLUE, null, null)
//        var series2Format = LineAndPointFormatter(Color.BLUE, Color.BLACK, null, null)
//
//        val bf = BarFormatter(Color.RED, Color.WHITE)
//        val bf2 = BarFormatter(Color.BLUE, Color.WHITE)
//        val bf3 = BarFormatter(Color.DKGRAY, Color.WHITE)
//
//        series2Format.linePaint.pathEffect = DashPathEffect(
//            floatArrayOf(
//                // always use DP when specifying pixel sizes, to keep things consistent across devices:
//                PixelUtils.dpToPix(10f), PixelUtils.dpToPix(10f)
//            ), 0f
//        )
//
//        series1Format.interpolationParams = CatmullRomInterpolator.Params(12,
//            CatmullRomInterpolator.Type.Centripetal)
//
//        seriesMidFormat.interpolationParams = CatmullRomInterpolator.Params(12,
//            CatmullRomInterpolator.Type.Centripetal)
//
//        series2Format.interpolationParams = CatmullRomInterpolator.Params(12,
//            CatmullRomInterpolator.Type.Centripetal)
//
//        barChart.addSeries(series1, bf)
//        barChart.addSeries(series2, bf2)
//        barChart.addSeries(seriesMd, bf3)
//
//        barChart.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
//            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
//                val i = (obj as Number).toFloat().roundToInt()
//                return toAppendTo.append(domainLabels[i])
//            }
//
//            override fun parseObject(source: String, pos: ParsePosition): Any? {
//                return null
//            }
//        }

        return binding.root
    }

    /**
     * This Helps us to logout through the current fragment
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val value = super.onOptionsItemSelected(item)
        if (item.itemId==R.id.logout_option_menu){
            viewModel.logout(binding.root)
            return true
        }
        return value
    }
}
