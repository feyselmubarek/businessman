package com.example.businessman.data

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Transaction(
    @PrimaryKey val id : Int = 0,
    val price : Double?,
    val transactionDate : String?,
    @Ignore val userPerform : User?,
    @Ignore val onProduct : Product? = null
) {
    constructor(
        id: Int = 0,
        price: Double?,
        transactionDate: String?
    ) : this(id, price, transactionDate, null, null)
}