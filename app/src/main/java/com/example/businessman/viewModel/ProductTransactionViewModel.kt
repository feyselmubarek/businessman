package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.businessman.data.*
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class ProductTransactionViewModel(application: Application): AndroidViewModel(application) {
    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)
    var sharedPreferences: SharedPreferences? = null

    var errorMessage = MutableLiveData("")
    var productQuantityField = ObservableField("")
    var productPriceField = ObservableField("")

    private var userId:Int = 0
    var productId:Int = 0
    lateinit var newTransactionProgressBar: ProgressBar

    fun addTransaction(){
        if (!repository.isConnected()) {
            repository.notConnected()
            return
        } else {
            repository.isConnected()
        }

        errorMessage.postValue("Processing...")
        sharedPreferences?.let {
            userId = it.getInt("user_id", 0)
        }

        val userPerform = User(id = userId, username = "", password = "", name = "")
        val onProduct = Product(id = productId, productName = "", productPrice = 0.toDouble(),
            productQuantity = 0, productImageString = "")

        val sdf = SimpleDateFormat("dd/M/yyyy")
        val currentDate = sdf.format(Date())
        println(" C DATE is  $currentDate")


        if (repository.isConnected()) {
            viewModelScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main) {
                    newTransactionProgressBar.visibility = View.VISIBLE
                }

                var price:Double? =  productPriceField.get()?.toDouble()
                var realPrice = price?.let { dao.getProductById(productId).productPrice?.minus(it) }
                var revertedPrice = realPrice?.times(-1)
                val proLoss = productQuantityField.get()?.toInt()?.let { revertedPrice?.times(it) }

                var transaction = Transaction(
                    price = proLoss,
                    onProduct = onProduct,
                    userPerform = userPerform,
                    transactionDate = currentDate
                )

                try {
                    var pro = Product(productId, "",
                        0.toDouble(), "", 0)

                    dao.getProductById(productId)?.let {
                        pro = Product(
                            id = it.id,
                            productName = it.productName,
                            productPrice = it.productPrice,
                            productImageString = it.productImageString,
                            productQuantity = productQuantityField.get()?.toInt()?.let { it1 ->
                                it.productQuantity?.minus(
                                    it1
                                )
                            }
                        )
                    }

                    repository.updateProduct(pro)
                    val response: Response<Transaction> = repository.addTransaction(transaction)
                    when {
                        response.isSuccessful -> {
                            withContext(Dispatchers.Main){
                                newTransactionProgressBar.visibility = View.INVISIBLE
                                Toast.makeText(getApplication(), "Done", Toast.LENGTH_SHORT).show()
                                errorMessage.postValue("Transaction Complete")
                            }
                            sharedPreferences?.let {
                                repository.syncDatabase(it.getInt("user_id", 0))
                                repository.syncTraDatabase(it.getInt("user_id", 0))
                            }
                        }

                        // Bad Request
                        response.code() == 400 -> {
                            withContext(Dispatchers.Main){
                                errorMessage.postValue("Transaction Failed")
                                newTransactionProgressBar.visibility = View.INVISIBLE
                            }
                        }

                        else -> {
                            errorMessage.postValue("Network Error")
                        }
                    }

                } catch (e:Exception){
                    withContext(Dispatchers.Main){
                        newTransactionProgressBar.visibility = View.INVISIBLE
                        errorMessage.postValue("")
                        Toast.makeText(getApplication(), "Connection Failed", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    }
}