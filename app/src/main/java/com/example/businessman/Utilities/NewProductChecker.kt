package com.example.businessman.Utilities

class NewProductChecker {
    companion object {

        fun checkProductPrice(price: Double): Boolean {
            return price > 0.0
        }

        fun checkProductQuantity(quantity: Int): Boolean {
            return quantity > 0
        }

        fun checkProductName(name: String): Boolean {
            return name.length < 20 && name.contains(Regex("[a-zA-Z]"))
        }
    }
}