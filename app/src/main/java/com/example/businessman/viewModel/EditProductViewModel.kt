/*
 * Copyright 2019, Developers team BusinessMan
 *
 */

package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.Product
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * A [EditProductViewModel] subclass of [AndroidViewModel] is used to render editFragment components
 */
class EditProductViewModel(application: Application) : AndroidViewModel(application) {

    // Values for database related tasks
    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)
    var sharedPreferences: SharedPreferences? = null

    // variables for binding into NewAccountFragment
    var productNameField = ObservableField("")
    var productPriceField = ObservableField("")
    var productQuantityField = ObservableField("")
    lateinit var editProductProgressBar: ProgressBar
    var productId:Int = 0

    fun updateProduct(){
        if (!repository.isConnected()) {
            repository.notConnected()
            return
        } else {
            repository.isConnected()
        }

        var product = Product(
            id = productId,
            productName = productNameField.get(),
            productPrice = productPriceField.get()?.toDouble(),
            productImageString = "Check",
            productQuantity = productQuantityField.get()?.toInt()
        )

        if (repository.isConnected()){
            viewModelScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main){
                    editProductProgressBar.visibility = View.VISIBLE
                }

                try {
                    val response : Response<Product> = repository.updateProduct(product)
                    when {
                        response.isSuccessful -> {
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Updated", Toast.LENGTH_SHORT).show()
                                editProductProgressBar.visibility = View.INVISIBLE
                            }
                            sharedPreferences?.let {
                                repository.syncDatabase(it.getInt("user_id", 0))
                            }
                        }

                        // Bad Request
                        response.code() == 400 ->{
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Error in update", Toast.LENGTH_SHORT).show()
                                editProductProgressBar.visibility = View.INVISIBLE
                            }
                        }

                        else -> {
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Network Error", Toast.LENGTH_SHORT).show()
                                editProductProgressBar.visibility = View.INVISIBLE
                            }
                        }
                    }

                } catch (e:Exception){
                    withContext(Dispatchers.Main){
                        Toast.makeText(getApplication(), "Connection Failed", Toast.LENGTH_SHORT).show()
                        editProductProgressBar.visibility = View.INVISIBLE
                    }
                }
            }

        } else {
            repository.notConnected()
        }
    }

    fun logout(view: View) {
        sharedPreferences?.let {
            with(it.edit()) {
                remove("user_id")
                putString("user_name", "")
                commit()
            }
        }

        repository.clearDatabase()
        Navigation.findNavController(view)
            .popBackStack()
    }

}