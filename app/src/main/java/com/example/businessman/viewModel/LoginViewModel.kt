/*
 * Copyright 2019, Developers team BusinessMan
 *
 */

package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.Navigator
import androidx.navigation.navOptions
import com.example.businessman.R
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.User
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.lang.Exception


/**
 * A [LoginViewModel] => subclass of [AndroidViewModel] for LoginFragment
 *
 */
class LoginViewModel(application: Application) : AndroidViewModel(application){

    // Values for database and argument related
    var sharedPreferences: SharedPreferences? = null
    private val apiServices:BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository:BMRepository = BMRepository(application, apiServices, dao)

    // values for view elements in LoginFragment
    val userNameField = ObservableField<String>("")
    val passwordField = ObservableField<String>("")
    val errorMessage = MutableLiveData<String>("")
    lateinit var progressBar:ProgressBar

    /**
     *
     */
    fun loginUser(view : View) {

        if (!repository.isConnected()) {
            repository.notConnected()
            return
        }else{
            repository.isConnected()
        }

        val user = User(username = userNameField.get(),
            password = passwordField.get(), name = "")
        val navigation = Navigation.findNavController(view)

//        repository.showUser(user)

        errorMessage.postValue("Connecting...")
        var response: Response<User>

        viewModelScope.launch(Dispatchers.IO) {
            withContext(Dispatchers.Main){
                progressBar.visibility = View.VISIBLE
            }

            try {
                response = apiServices.loginUserAsync(user).await()

                when {
                    response.isSuccessful -> {
                        errorMessage.postValue("Success")
                        saveUser(response.body())

                        // Updating The UI by using mainThread
                        withContext(Dispatchers.Main){
                            loggedIn(response.body(), navigation)
                        }
                    }

                    response.code() == 401 -> {
                        errorMessage.postValue("Incorrect Login Information")
                        withContext(Dispatchers.Main){
                            progressBar.visibility = View.INVISIBLE
                        }
                    }

                    else -> {
                        Toast.makeText(getApplication(), "Network Error On Login",
                                Toast.LENGTH_SHORT).show()
                        errorMessage.postValue("")
                    }
                }

            }catch (e:Exception){
                withContext(Dispatchers.Main){
                    progressBar.visibility = View.INVISIBLE
                    errorMessage.postValue("")
                    Toast.makeText(getApplication(), "Connection Failed, Try again!",
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    /**
     *
     */
    private fun loggedIn(user: User?, navigation: NavController) {
        progressBar.visibility = View.INVISIBLE
        User.loggedInUser = user
        val options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }

        val  userId = user?.id
        val arg = Bundle()

        if (userId != null) {
            arg.putInt("userId", userId)
        }
        navigation.navigate(R.id.productsFragmentDest, arg, options)
    }

    /**
     *
     */
    private fun saveUser(user: User?){
        user.let {
            sharedPreferences?.let {
                with(it.edit()){
                    if (user != null) {
                        putInt("user_id", user.id)
                        putString("user_name", user.name)
                        putString("user_username", user.username)
                        commit()
                    }
                }
            }
        }
    }

    /**
     *
     */
    fun logInOffline(view: View) {
        sharedPreferences?.let {
            val userId = it.getInt("user_id", 0)
            val name = it.getString("user_name", "")
            val username = it.getString("user_username", "")

            if (userId != 0 && name.isNotEmpty()) {
                User.loggedInUser = User(id = userId, username = username,
                    password = null, name = name)
                val navigation = Navigation.findNavController(view)
                loggedIn(User.loggedInUser, navigation)
            }
        }
    }

}