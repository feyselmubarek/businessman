package com.example.businessman.EndToEndTesting

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import com.example.businessman.MainActivity
import com.example.businessman.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class BusinessManEndToEndTesting {
    @Test
    fun testLogin(){
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.username)).perform(typeText("feysel"))

        onView(withId(R.id.password_et)).perform(typeText("faya"))

        onView(withId(R.id.signInButton)).perform(click())

        onView(withId(R.id.product_recycler_view)).check(matches(isDisplayed()))
    }

    @Test
    fun testRegister(){
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.new_account_create_TV)).perform(click())

        onView(withId(R.id.username)).perform(typeText("Habte"))

        onView(withId(R.id.create_password_tv)).perform(typeText("Asefa"))

        onView(withId(R.id.create_confirm_password_tv)).perform(typeText("Asefa"))

        onView(withId(R.id.name_tv)).perform(typeText("Habte Asefa"))

        onView(withId(R.id.new_product_fab)).perform(click())

        onView(withId(R.id.error_message_TV)).check(matches(withText("Registered ")))
    }
}