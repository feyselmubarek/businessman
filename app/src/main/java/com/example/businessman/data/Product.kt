package com.example.businessman.data

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class Product(
    @PrimaryKey val id:Int = 0,
    val productName : String?,
    val productPrice : Double?,
    var productImageString : String?,
    val productQuantity : Int?,
    @Ignore val user: User? = null
){
    constructor(
        id: Int = 0,
        productName: String?,
        productPrice: Double?,
        productImageString: String?,
        productQuantity: Int?
    ) : this(id, productName, productPrice, productImageString, productQuantity, null)
}