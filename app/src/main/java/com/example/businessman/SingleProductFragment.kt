/*
 * Copyright 2019, Development team BusinessMan
 *
 *
 */

package com.example.businessman

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.navOptions
import com.example.businessman.databinding.FragmentSingleProductBinding
import com.example.businessman.viewModel.SingleProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_single_product.*
import kotlinx.android.synthetic.main.fragment_single_product.view.*
import kotlinx.android.synthetic.main.fragment_single_product.view.imageView3

/**
 * A simple [Fragment] subclass [SingleProductFragment].
 *
 */
class SingleProductFragment : Fragment() {
    // variable for binding and viewModel
    lateinit var viewModel: SingleProductViewModel
    lateinit var binding: FragmentSingleProductBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_single_product, container, false)
        viewModel = ViewModelProviders.of(this).get(SingleProductViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.sellFloatingActionButton = binding.root.item_sell_fab_btn
        viewModel.deleteFloatingActionButton = binding.root.item_dell_fab_btn
        viewModel.editProductFloatingActionButton = binding.root.add_item_product_fab_btn
        viewModel.imageView = binding.root.imageView3


        setHasOptionsMenu(true)

        arguments?.let {
            val id = it.getInt("product_Id")

            val options = navOptions {
                anim {
                    enter = R.anim.slide_in_right
                    exit = R.anim.slide_out_left
                    popEnter = R.anim.slide_in_left
                    popExit = R.anim.slide_out_right
                }
            }

            val arg = Bundle()
            arg.putInt("product_Id", id)

            viewModel.editProductFloatingActionButton.setOnClickListener {
                Navigation.findNavController(binding.root).navigate(R.id.editProductFragmentDest, arg, options)
            }

            viewModel.sellFloatingActionButton.setOnClickListener {
                Navigation.findNavController(binding.root).navigate(R.id.productTransactionFragmentDest, arg, options)
            }

            viewModel.deleteFloatingActionButton.setOnClickListener{
                viewModel.deleteProduct(id)
            }

            viewModel.getMyProductFromDao(id)
            viewModel.product.observe(viewLifecycleOwner,
                Observer { product ->
                    run {
                        binding.singleProductNameTv.text = product.productName
                        binding.singleProductQuantityTv.text = product.productQuantity.toString()
                        binding.singleProductNameTextView.text = product.productName
                        binding.singleProductPriceTv.text = product.productPrice.toString()
                        Picasso.get().load("http://10.6.250.131:5002/Images/" + product.productImageString)
                            .resize(415, 250).into(imageView3)
                    }
                })
        }
        return binding.root
    }

    /**
     * This Helps us to logout through the current fragment
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val value = super.onOptionsItemSelected(item)
        if (item.itemId==R.id.logout_option_menu){
            viewModel.logout(binding.root)
            return true
        }
        return value
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        println("id is $id from onActivityResult SingProFrag")
        viewModel.syncDatabase(id)
        viewModel.getMyProductFromDao(id)
        viewModel.product.observe(viewLifecycleOwner,
            Observer { product ->
                run {
                    binding.singleProductNameTv.text = product.productName
                    binding.singleProductQuantityTv.text = product.productQuantity.toString()
                    binding.singleProductNameTextView.text = product.productName
                    binding.singleProductPriceTv.text = product.productPrice.toString()
                    Picasso.get().load("http://10.6.250.131:5002/Images/" + product.productImageString)
                        .resize(415, 250).into(imageView3)
                }
            })
    }
}
