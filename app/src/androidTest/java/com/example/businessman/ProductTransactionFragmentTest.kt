package com.example.businessman

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ProductTransactionFragmentTest{
    @Test
    fun testDisplay(){
        launchFragmentInContainer<ProductTransactionFragment>()

        Espresso.onView(ViewMatchers.withId(R.id.imageView4))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.payment_amount_et))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.name_tv))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.new_product_fab))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testContent(){
        launchFragmentInContainer<ProductTransactionFragment>()
        val context = ApplicationProvider.getApplicationContext<MainActivity>()

        Espresso.onView(ViewMatchers.withId(R.id.payment_amount_et))
            .check(ViewAssertions.matches(ViewMatchers.withText(context.resources.getString(R.string.product_name))))

        Espresso.onView(ViewMatchers.withId(R.id.name_tv))
            .check(ViewAssertions.matches(ViewMatchers.withText(context.resources.getString(R.string.product_quantity))))
    }

}