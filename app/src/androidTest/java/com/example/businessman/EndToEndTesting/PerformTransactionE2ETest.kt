package com.example.businessman.EndToEndTesting

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import com.example.businessman.MainActivity
import com.example.businessman.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class PerformTransactionE2ETest {
    @Test
    fun performAction(){

        ActivityScenario.launch(MainActivity::class.java)

        Espresso.onView(ViewMatchers.withId(R.id.username)).perform(ViewActions.typeText("feysel"))

        Espresso.onView(ViewMatchers.withId(R.id.password_et)).perform(ViewActions.typeText("faya"))

        Espresso.onView(ViewMatchers.withId(R.id.signInButton)).perform(ViewActions.click())


        Espresso.onView(ViewMatchers.withId(R.id.product_recycler_view))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.layout.my_product_recycler_view_item)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.item_sell_fab_btn)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.payment_amount_et)).perform(ViewActions.typeText("100"))

        Espresso.onView(ViewMatchers.withId(R.id.name_tv)).perform(ViewActions.typeText("1"))

        Espresso.onView(ViewMatchers.withId(R.id.error_message_TV))
            .check(ViewAssertions.matches(ViewMatchers.withText("Added new Product")))

    }
}
