package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.Product
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.*
import retrofit2.Response

class SingleProductViewModel(application: Application) : AndroidViewModel(application){

    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)

    var sharedPreferences: SharedPreferences? = null
    var productName : MutableLiveData<String> = MutableLiveData("")
    var productPrice : MutableLiveData<String> = MutableLiveData("")
    var productQuantity : MutableLiveData<String> = MutableLiveData("")
    var product: MutableLiveData<Product> = MutableLiveData()

    lateinit var sellFloatingActionButton: FloatingActionButton
    lateinit var deleteFloatingActionButton: FloatingActionButton
    lateinit var editProductFloatingActionButton: FloatingActionButton
    lateinit var imageView: ImageView

    fun logout(view: View) {
        sharedPreferences?.let {
            with(it.edit()) {
                remove("user_id")
                putString("user_name", "")
                commit()
            }
        }
        repository.clearDatabase()
        Navigation.findNavController(view)
            .popBackStack()
    }

    fun getMyProductFromDao(userId: Int){
        GlobalScope.async {
            dao.getProductById(userId)?.let {
                product.postValue(it)
                productName.postValue(it.productName)
                productPrice.postValue(it.productPrice.toString())
                productQuantity.postValue(it.productQuantity.toString())
            }
        }
    }

    fun deleteProduct(productId:Int){
        if (repository.isConnected()) {
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    val response: Response<Product> = repository.deleteProduct(productId)
                    when {
                        response.isSuccessful -> {
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Deleted", Toast.LENGTH_SHORT).show()
                            }
                        }

                        // Bad Request
                        response.code() == 400 ->{
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Delete Failed", Toast.LENGTH_SHORT).show()
                            }
                        }

                        else -> {
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Network Error", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                } catch (e: Exception){
                    withContext(Dispatchers.Main){
                        Toast.makeText(getApplication(), "Connection Failed, Try again!",
                            Toast.LENGTH_SHORT).show()
                    }
                }

                dao.deleteProduct(productId)
            }

        } else { repository.notConnected() }
    }

    fun syncDatabase(id:Int){
        repository.syncDatabase(id)
    }
}