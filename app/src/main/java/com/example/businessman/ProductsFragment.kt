package com.example.businessman

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.businessman.databinding.FragmentProductsBinding
import com.example.businessman.viewModel.ProductsViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * A simple [Fragment] subclass [ProductsFragment] used for rendering all of users products
 *
 */
class ProductsFragment : Fragment() {

    lateinit var recyclerViewAdapter: MyProductsAdapter
    lateinit var viewModel:ProductsViewModel
    lateinit var binding: FragmentProductsBinding

    /**
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProductsViewModel::class.java)
        recyclerViewAdapter = MyProductsAdapter(listOf())
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
    }

    /**
     *
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false)
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        // Set the adapter
        val recyclerView = binding.productRecyclerView

        recyclerView.layoutManager = GridLayoutManager(binding.root.context, 2)
        recyclerView.adapter = recyclerViewAdapter

        binding.root.findViewById<FloatingActionButton>(R.id.new_product_fab)?.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_productsFragmentDest_to_newProductFragment, null)
        )

        return binding.root
    }

    /**
     * This override function used as to observe product changes
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let {
            val userId = it.getInt("userId")
            viewModel.getMyProducts(userId)
            viewModel.products.observe(
                viewLifecycleOwner,
                Observer { products -> recyclerViewAdapter.updateProducts(products) })
        }
    }

    /**
     * This Helps us to logout through the current fragment
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val value = super.onOptionsItemSelected(item)
        if (item.itemId==R.id.logout_option_menu){
            viewModel.logout(binding.root)
            return true
        }
        return value
    }
}
