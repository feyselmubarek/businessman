package com.example.businessman

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.navigation.navOptions
import androidx.recyclerview.widget.RecyclerView
import com.example.businessman.data.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.my_product_recycler_view_item.view.*

class MyProductsAdapter(private var products: List<Product>): RecyclerView.Adapter<MyProductsAdapter.ViewHolder>() {

    fun updateProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.my_product_recycler_view_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = products.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = products[position]

        val price = product.productPrice.toString()
        val name = product.productName

        holder.view.setOnClickListener {
                val options = navOptions {
                    anim {
                        enter = R.anim.slide_in_right
                        exit = R.anim.slide_out_left
                        popEnter = R.anim.slide_in_left
                        popExit = R.anim.slide_out_right
                    }
                }
                val  productId = product.id
                val arg = Bundle()

            arg.putInt("product_Id", productId)

            Navigation.findNavController(holder.view).navigate(R.id.singleProductFragmentDest, arg, options)
        }

        with(holder) {
            productName.text = name
            productPrice.text = "$$price"
                try {
                    Picasso.get().load("http://10.6.250.131:5002/Images/" + product.productImageString)
                        .into(productImageView)
                }catch (e:Exception){
                    println()
                    println(e.message)
                    println()
            }
        }
    }

    inner class ViewHolder(val view:View):RecyclerView.ViewHolder(view){
        val productName: TextView = view.myProductName
        val productPrice: TextView = view.myProductPrice
        val productImageView: ImageView = view.product_image
    }
}