package com.example.businessman.EndToEndTesting

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import com.example.businessman.MainActivity
import com.example.businessman.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class AddAccountE2ETest {
    @Test
    fun addAccount(){

        ActivityScenario.launch(MainActivity::class.java)

        Espresso.onView(ViewMatchers.withId(R.id.new_account_create_TV)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.newAccountFragment))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.username)).perform(ViewActions.typeText("feysel"))

        Espresso.onView(ViewMatchers.withId(R.id.create_password_tv)).perform(ViewActions.typeText("faya"))

        Espresso.onView(ViewMatchers.withId(R.id.create_confirm_password_tv)).perform(ViewActions.typeText("faya"))

        Espresso.onView(ViewMatchers.withId(R.id.name_tv)).perform(ViewActions.typeText("name_tv"))

        Espresso.onView(ViewMatchers.withId(R.id.new_product_fab)).perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.error_message_TV))
            .check(ViewAssertions.matches(ViewMatchers.withText("Done")))

    }
}