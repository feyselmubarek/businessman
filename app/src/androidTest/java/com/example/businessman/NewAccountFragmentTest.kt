package com.example.businessman

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@RunWith(AndroidJUnit4::class)
class NewAccountFragmentTest{

    @Test
    fun testDisplay() {

        launchFragmentInContainer<NewProductFragment>()

        onView(withId(R.id.username))
            .check(matches(isDisplayed()))

        onView(withId(R.id.create_password_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.create_confirm_password_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.name_tv))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testContent(){
        launchFragmentInContainer<LoginFragment>()

        onView(withId(R.id.business_man_logo_tv))
            .check(matches(withText(R.string.app_name)))

        onView(withId(R.id.signInButton))
            .check(matches(withText(R.string.login_button)))

        onView(withId(R.id.new_account_create_TV))
            .check(matches(withText(R.string.new_account)))
    }

}