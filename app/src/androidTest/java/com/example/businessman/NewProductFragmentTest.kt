package com.example.businessman

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class NewProductFragmentTest {
    @Test
    fun testDisplay(){

        launchFragmentInContainer<NewProductFragment>()

        onView(withId(R.id.create_product_name_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.create_product_price_TV))
            .check(matches(isDisplayed()))

        onView(withId(R.id.create_product_quantity_tv))
            .check(matches(isDisplayed()))

        onView(withId(R.id.imageView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testContent(){
        launchFragmentInContainer<NewProductFragment>()
        val context = getApplicationContext<MainActivity>()
        onView(withId(R.id.create_product_name_tv))
            .check(matches(withText(context.resources.getString(R.string.product_name))))

        onView(withId(R.id.create_product_quantity_tv))
            .check(matches(withText(context.resources.getString(R.string.product_quantity))))
    }
}