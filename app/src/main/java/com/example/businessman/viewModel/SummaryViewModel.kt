package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.graphics.Color
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.androidplot.xy.*
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.Product
import com.example.businessman.data.Transaction
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.FieldPosition
import java.text.Format
import java.text.ParsePosition
import kotlin.math.roundToInt

class SummaryViewModel (application: Application) : AndroidViewModel(application){

    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)
    var transactions: LiveData<List<Transaction>> = MutableLiveData(listOf())
    var sharedPreferences: SharedPreferences? = null

    lateinit var barChart: XYPlot
    lateinit var plotChart: XYPlot

    var domainLabels = listOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
    var series1: ArrayList<Double> = ArrayList()
    var series2: ArrayList<Double> = ArrayList()
    var series3: ArrayList<Double> = ArrayList()

    fun logout(view: View) {
        sharedPreferences?.let {
            with(it.edit()) {
                remove("user_id")
                putString("user_name", "")
                commit()
            }
        }

        repository.clearDatabase()
        Navigation.findNavController(view)
            .popBackStack()
    }

    fun getMyTransaction(userId: Int) {
//        GlobalScope.launch {
//            repository.getAllTransaction(userId)?.let {
//                transactions = it
//            }
//        }
        makeBarGraph()
    }

    fun makeBarGraph(){
        var sum1 = 0.0
        var sum2 = 0.0
        var sum3 = 0.0

       var series1Numbers = listOf(1, 4, 2, 8, 4, 16, 8, 32, 16, 64, 11, 12)
       var seriesMidNumbers = listOf(1, 2, 1, 4, 2, 10, 5, 20, 6, 24, 11, 12)
       var series2Numbers = listOf(5, 2, 10, 5, 20, 10, 40, 20, 80, 40, 11, 12)

//        for (x in 0 until 12){
//            for (item in transactions){
//                var indexHolder = item.transactionDate?.split('/')
//                var index = indexHolder?.get(1)?.toInt()
//                println("The index: $index")
//
//                if (x == index){
//                        when {
//                            item.price!! < 0 -> sum2 += item.price.times(-1)
//                            item.price!! > 0 -> sum1 += item.price
//                            else -> { sum3 += item.price }
//                        }
//                    }
//                }
//
//            series1.add(x, sum1)
//            series2.add(x, sum2)
//            series3.add(x, sum3)
//        }

        val seriesSim1 = SimpleXYSeries(series1Numbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Prof")
        val seriesSim2 = SimpleXYSeries(seriesMidNumbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Loss")
        val seriesSim3 = SimpleXYSeries(series2Numbers, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Aver")

        val bf = BarFormatter(Color.RED, Color.WHITE)
        val bf2 = BarFormatter(Color.BLUE, Color.WHITE)
        val bf3 = BarFormatter(Color.DKGRAY, Color.WHITE)

        barChart.addSeries(seriesSim1, bf)
        barChart.addSeries(seriesSim2, bf2)
        barChart.addSeries(seriesSim3, bf3)

        barChart.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(domainLabels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any? {
                return null
            }
        }
    }
}