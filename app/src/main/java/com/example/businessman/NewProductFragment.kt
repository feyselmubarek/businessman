package com.example.businessman

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.loader.content.CursorLoader
import com.example.businessman.databinding.FragmentNewProductFragmentBinding
import com.example.businessman.viewModel.NewProductViewModel
import kotlinx.android.synthetic.main.fragment_new_product_fragment.view.*
import java.io.File

private const val MY_PERMISSION_REQUEST:Int = 100
private const val PICK_IMAGE_FROM_GALLERY:Int = 1

/**
 * A simple [Fragment] subclass. [NewProductFragment] is used to render newProductFragment elements
 *
 */
class NewProductFragment : Fragment() {

    private lateinit var binding: FragmentNewProductFragmentBinding
    private lateinit var viewModel: NewProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // This is used to make read permission for the fragment
        if (context?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)
            } != PackageManager.PERMISSION_DENIED){

            activity?.let {
                ActivityCompat.requestPermissions(
                    it,
                    listOf(android.Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),
                    MY_PERMISSION_REQUEST)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_product_fragment, container, false)
        viewModel = ViewModelProviders.of(this).get(NewProductViewModel::class.java)
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.newProductProgressBar = binding.root.newProductProgressBar
        viewModel.newProductProgressBar.visibility = View.INVISIBLE

        // On click listener for the image picking button
        binding.root.pick_button.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT

            startActivityForResult(Intent.createChooser(intent, "Select Image"),
                PICK_IMAGE_FROM_GALLERY)
        }

        binding.viewModel = viewModel
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK
            && data != null && data.data != null){
            val imageUri = data?.data
            val file = File(getAbsolutePathFromUri(imageUri))

            viewModel.productImageNameField.set(file.name)
            viewModel.cont = context?.contentResolver?.getType(imageUri).toString()
            viewModel.imageFile = file
        }
    }

    /**
     * Helper function
     * Getting absolute path of file from uri object
     */
    private fun getAbsolutePathFromUri(uri: Uri) : String{
        var projection = listOf(MediaStore.Images.Media.DATA).toTypedArray()

        var loader = context?.let { CursorLoader(
            it, uri, projection, null, null, null)
        }

        var cursor = loader?.loadInBackground()
        var columnIndex: Int? = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor?.moveToFirst()
        var result = columnIndex?.let { cursor?.getString(it) }
        cursor?.close()

        return result.toString()
    }
}
