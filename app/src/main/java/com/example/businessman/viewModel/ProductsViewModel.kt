package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.example.businessman.R
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.Product
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository

class ProductsViewModel(application: Application): AndroidViewModel(application) {

    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)
    var products: LiveData<List<Product>> = MutableLiveData(listOf())
    var sharedPreferences: SharedPreferences? = null

    fun logout(view: View) {
        sharedPreferences?.let {
            with(it.edit()) {
                remove("user_id")
                putString("user_name", "")
                commit()
            }
        }

        repository.clearDatabase()
        Navigation.findNavController(view)
            .popBackStack()
    }

    fun getMyProducts(userId: Int) {
        products = repository.getMyProducts(userId)
    }
}