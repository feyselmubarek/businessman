package com.example.businessman


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.businessman.databinding.FragmentEditProductBinding
import com.example.businessman.viewModel.EditProductViewModel
import com.example.businessman.viewModel.NewAccountViewModel
import com.example.businessman.viewModel.ProductsViewModel


/**
 * A simple [Fragment] subclass.
 *
 */
class EditProductFragment : Fragment() {
    private lateinit var binding: FragmentEditProductBinding
    private lateinit var viewModel: EditProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this).get(EditProductViewModel::class.java)
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_product, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.editProductProgressBar = binding.saleProgressBar
        viewModel.editProductProgressBar.visibility = View.INVISIBLE
        binding.viewModel = viewModel
        return binding.root
    }

    /**
     * This Helps us to logout through the current fragment
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val value = super.onOptionsItemSelected(item)
        if (item.itemId==R.id.logout_option_menu){
            viewModel.logout(binding.root)
            return true
        }
        return value
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let {
            var id = it.getInt("product_Id")
            viewModel.productId = id
        }
    }
}
