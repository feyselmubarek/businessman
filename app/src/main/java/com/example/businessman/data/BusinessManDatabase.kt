package com.example.businessman.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Product::class, Transaction::class], version = 1, exportSchema = false)
abstract class BusinessManDatabase :RoomDatabase(){

    abstract fun getBusinessManDao() : BusinessManDao

    companion object {
        var TEST_MODE = false
        @Volatile
        private var INSTANCE: BusinessManDatabase? = null

        fun getDatabase(context: Context): BusinessManDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BusinessManDatabase::class.java,
                    "business_man_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}