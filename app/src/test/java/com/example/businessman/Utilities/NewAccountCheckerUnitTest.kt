package com.example.businessman.Utilities

import org.junit.Test

import org.junit.Assert.*

class NewAccountCheckerUnitTest {

    @Test
    fun checkPasswordStrength() {
        val incorrectPassword = "aaaa"
        val incorrectResult = NewAccountChecker.checkPasswordStrength(incorrectPassword)
        val correctPassword = "fei86Tgs"
        val correctResult = NewAccountChecker.checkPasswordStrength(correctPassword)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun checkUserName() {
        val correctUserName = "feysel"
        val correctResult = NewAccountChecker.checkUserName(correctUserName)
        val incorrectUserName = "Ermiyas"
        val incorrectResult = NewAccountChecker.checkUserName(incorrectUserName)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun checkProductName() {
        val incorrectProductName = "908"
        val incorrectProductName2 = "````````````````````````````````````"

        val incorrectResult = NewAccountChecker.checkProductName(incorrectProductName)
        val incorrectResult2 = NewAccountChecker.checkProductName(incorrectProductName2)

        assert(!incorrectResult)
        assert(!incorrectResult2)
    }
}