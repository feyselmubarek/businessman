package com.example.businessman.Utilities

import org.junit.Test

class LoginInfoCheckerUnitTest{
    @Test
    fun testPasswordStrength(){
        val incorrectPassword = "aaaa"
        val incorrectResult = LoginInfoChecker.checkPasswordStrength(incorrectPassword)
        val correctPassword = "fei86Tgs"
        val correctResult = LoginInfoChecker.checkPasswordStrength(correctPassword)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testUserName(){
        val correctUserName = "feysel"
        val correctResult = LoginInfoChecker.checkUserName(correctUserName)
        val incorrectUserName = "Ermiyas"
        val incorrectResult = LoginInfoChecker.checkUserName(incorrectUserName)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductName(){
        val incorrectProductName = "908"
        val incorrectProductName2 = "````````````````````````````````````"

        val incorrectResult = LoginInfoChecker.checkProductName(incorrectProductName)
        val incorrectResult2 = LoginInfoChecker.checkProductName(incorrectProductName2)

        assert(!incorrectResult)
        assert(!incorrectResult2)
    }
}