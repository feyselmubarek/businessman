package com.example.businessman.data

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BusinessManDatabaseTest{

    private var businessManDao:BusinessManDao? = null

    @Before
    fun setup() {
        BusinessManDatabase.TEST_MODE = true
        businessManDao = BusinessManDatabase
            .getDatabase(InstrumentationRegistry.getTargetContext()).getBusinessManDao()
    }

    @After
    fun tearDown() {

    }

    @Test
    fun shouldInsertProductItem() {
        val product = Product(66, "Banana", 25.0,  "", 3)
        businessManDao?.insertProduct(product)
        val productTest = businessManDao?.getProductById(product.id)
        assertEquals(product.productName, product.productName)
    }

    @Test
    fun should_Flush_All_Data(){
        businessManDao?.deleteAllProduct()
        assertEquals(businessManDao?.getAllProducts(), 0)
    }
}