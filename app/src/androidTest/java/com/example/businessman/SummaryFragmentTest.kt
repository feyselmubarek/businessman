package com.example.businessman

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

/**
 * A test using the androidx.test unified API, which can execute on an Android device or locally using Robolectric.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class SummaryFragmentTest{
    @Test
    fun testDisplay() {

        launchFragmentInContainer<SummaryFragment>()

        Espresso.onView(ViewMatchers.withId(R.id.barChart))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.plotChart))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}