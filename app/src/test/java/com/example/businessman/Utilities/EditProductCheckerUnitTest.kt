package com.example.businessman.Utilities

import org.junit.Assert.*
import org.junit.Test

class EditProductCheckerUnitTest{
    @Test
    fun testProductPrice(){
        val incorrectPassword = -0.8
        val incorrectResult = EditProductChecker.checkProductPrice(incorrectPassword)
        val correctPassword = 9.0
        val correctResult = EditProductChecker.checkProductPrice(correctPassword)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductQuantity(){
        val correctUserName = 7
        val correctResult = EditProductChecker.checkProductQuantity(correctUserName)
        val incorrectUserName = -10
        val incorrectResult = EditProductChecker.checkProductQuantity(incorrectUserName)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductName(){
        val incorrectProductName = "908"
        val incorrectProductName2 = "````````````````````````````````````"

        val incorrectResult = EditProductChecker.checkProductName(incorrectProductName)
        val incorrectResult2 = EditProductChecker.checkProductName(incorrectProductName2)

        assert(!incorrectResult)
        assert(!incorrectResult2)
    }
}