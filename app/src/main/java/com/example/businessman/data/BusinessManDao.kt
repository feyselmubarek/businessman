package com.example.businessman.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface BusinessManDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(vararg product: Product)

    @Query(" SELECT * FROM product WHERE id = :id")
    fun getProductById(id: Int): Product

    @Query(" SELECT * FROM product ")
    fun getAllProducts(): LiveData<List<Product>>

    @Query(" DELETE  FROM product ")
    fun deleteAllProduct()

    @Update
    fun updateProduct(product: Product): Int

    @Query(" DELETE  FROM product WHERE id = :id ")
    fun deleteProduct(id: Int) : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTransaction(vararg transaction: Transaction)

    @Query("SELECT * FROM `transaction`")
    fun getAllTransaction() : LiveData<List<Transaction>>

    @Query(" DELETE  FROM `transaction` ")
    fun deleteAllTransaction()
}