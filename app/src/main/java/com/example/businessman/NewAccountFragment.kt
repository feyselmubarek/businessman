package com.example.businessman


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.businessman.databinding.FragmentNewAccountBinding
import com.example.businessman.viewModel.NewAccountViewModel
import kotlinx.android.synthetic.main.fragment_new_account.view.*


/**
 * A simple [Fragment] subclass.
 *
 */
class NewAccountFragment : Fragment() {
    private lateinit var binding: FragmentNewAccountBinding
    private lateinit var viewModel: NewAccountViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_account, container, false)
        viewModel = ViewModelProviders.of(this).get(NewAccountViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.newAccountProgressBar = binding.root.new_account_progressBar
        viewModel.newAccountProgressBar.visibility = View.INVISIBLE
        binding.viewModel = viewModel

        return binding.root
    }


}
