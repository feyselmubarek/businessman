/*
 * Copyright 2019, Developers team BusinessMan
 *
 */

package com.example.businessman.viewModel

import android.app.Application
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.os.Build
import android.util.Base64
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.Product
import com.example.businessman.data.User
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


/**
 * A [NewAccountViewModel] subclass of [AndroidViewModel] is used to render newAccount Fragment components
 */
class NewProductViewModel(application: Application): AndroidViewModel(application) {
    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)
    var sharedPreferences: SharedPreferences? = null

    var errorMessage = MutableLiveData("")
    var productNameField = ObservableField("")
    var productQuantityField = ObservableField("")
    var productPriceField = ObservableField("")
    var productImageNameField = ObservableField("")

    lateinit var imageFile:File
    lateinit var cont:String

    private var userId:Int = 0
    lateinit var newProductProgressBar: ProgressBar

    /**
     * A function to add new products for logged in user
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun addProduct() {

        if (!repository.isConnected()) {
            repository.notConnected()
            return
        } else {
            repository.isConnected()
        }

        errorMessage.postValue("Processing...")
        sharedPreferences?.let {
            userId = it.getInt("user_id", 0)
        }

        val userPerform = User(id = userId, username = "", password = "", name = "")
        var product = Product(
            productName = productNameField.get(),
            productPrice = productPriceField.get()?.toDouble(),
            productQuantity = productQuantityField.get()?.toInt(),
            productImageString = imageFile.name, user = userPerform
        )

        if (repository.isConnected()) {
            viewModelScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main){
                    newProductProgressBar.visibility = View.VISIBLE
                }

                try {
                    // Getting image
                    val originalFile = imageFile
                    val bytes = File(originalFile.path).readBytes()
                    val base64 = Base64.encodeToString(bytes, Base64.DEFAULT)

                    product.productImageString = base64
                    val response: Response<Product> = repository.addProduct(product)

                    when {
                        response.isSuccessful -> {
                            withContext(Dispatchers.Main){
                                newProductProgressBar.visibility = View.INVISIBLE
                                Toast.makeText(getApplication(), "Done", Toast.LENGTH_SHORT).show()
                                errorMessage.postValue("Added New Product")
                            }
                        }

                        // Bad Request
                        response.code() == 400 -> {
                            withContext(Dispatchers.Main){
                                errorMessage.postValue("Product Already Exists")
                                newProductProgressBar.visibility = View.INVISIBLE
                            }
                        }

                        else -> {
                            errorMessage.postValue("Network Error")
                        }
                    }

                } catch (e:Exception) {
                    withContext(Dispatchers.Main){
                        newProductProgressBar.visibility = View.INVISIBLE
                        errorMessage.postValue("")
                        Toast.makeText(getApplication(), "Connection Failed", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } else {
            repository.notConnected()
        }
    }
}