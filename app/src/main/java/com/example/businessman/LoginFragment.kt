/*
 * Copyright 2019, Developers team BusinessMan
 *
 */

package com.example.businessman

import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.businessman.databinding.FragmentLoginBinding
import com.example.businessman.viewModel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.view.*

/**
 * [LoginFragment] => A simple [Fragment] subclass, which is and used for rendering login
 * fragment components
 *
 * Users login here by typing password and username
 */
class LoginFragment : Fragment() {

    // Data binding
    private lateinit var binding: FragmentLoginBinding

    // view Model for the LoginFragment
    private lateinit var viewModel: LoginViewModel

    /**
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        viewModel.sharedPreferences = context?.getSharedPreferences("USERDATA",
            Context.MODE_PRIVATE)
    }

    /**
     *
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container,
            false)
        viewModel.progressBar = binding.root.login_progress_bar
        viewModel.progressBar.visibility = View.INVISIBLE
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        binding.root.findViewById<TextView>(R.id.new_account_create_TV)?.setOnClickListener(
            Navigation
                .createNavigateOnClickListener(R.id.action_loginFragment_to_newAccountFragment,
                    null)
        )
        return binding.root
    }

    /**
     *
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.logInOffline(binding.root)
    }
}
