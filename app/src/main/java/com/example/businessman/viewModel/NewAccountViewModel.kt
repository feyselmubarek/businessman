/*
 * Copyright 2019, Developers team BusinessMan
 *
 */

package com.example.businessman.viewModel

import android.app.Application
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.BusinessManDatabase
import com.example.businessman.data.User
import com.example.businessman.network.BMAPIServices
import com.example.businessman.repository.BMRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.lang.Exception

/**
 * A [NewAccountViewModel] subclass of [AndroidViewModel] is used to render newAccount Fragment components
 */
class NewAccountViewModel(application: Application):AndroidViewModel(application) {

    // Values for database related tasks
    private val apiServices: BMAPIServices = BMAPIServices.getInstance()
    private val dao: BusinessManDao = BusinessManDatabase.getDatabase(application).getBusinessManDao()
    private val repository: BMRepository = BMRepository(application, apiServices, dao)

    // variables for binding into NewAccountFragment
    var errorMessage = MutableLiveData("")
    var userNameField = ObservableField("")
    var passwordField = ObservableField("")
    var confirmPasswordField = ObservableField("")
    var nameField = ObservableField("")
    lateinit var newAccountProgressBar: ProgressBar

    /**
     * Function for registering new users to the system
     */
    fun registerUser() {

        if (!repository.isConnected()) {
            repository.notConnected()
            return
        } else {
            repository.isConnected()
        }

        if(!passwordField.get().equals(confirmPasswordField.get())){
            errorMessage.postValue("Not matching passwords")
            return
        }

        errorMessage.postValue("Progressing...")
        var user = User(username = userNameField.get(), password = passwordField.get(),
            name = nameField.get().toString())

        if (repository.isConnected()) {
            viewModelScope.launch(Dispatchers.IO) {
                withContext(Dispatchers.Main){
                    newAccountProgressBar.visibility = View.VISIBLE
                }

                try {
                    val response: Response<User> = repository.addUser(user)

                    when {
                        response.isSuccessful -> {
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Registered", Toast.LENGTH_SHORT).show()
                                errorMessage.postValue("Registered")
                                newAccountProgressBar.visibility = View.INVISIBLE
                            }
                        }

                        // Bad Request
                        response.code() == 400 ->{
                            errorMessage.postValue("Username is Already InUse")
                            withContext(Dispatchers.Main){
                                newAccountProgressBar.visibility = View.INVISIBLE
                            }
                        }

                        else -> {
                            withContext(Dispatchers.Main){
                                Toast.makeText(getApplication(), "Network Error", Toast.LENGTH_SHORT).show()
                                errorMessage.postValue("Network Error")
                                newAccountProgressBar.visibility = View.INVISIBLE
                            }
                        }
                    }
                } catch (e:Exception){
                    withContext(Dispatchers.Main){
                        newAccountProgressBar.visibility = View.INVISIBLE
                        errorMessage.postValue("")
                        Toast.makeText(getApplication(), "Connection Failed, Try again!",
                            Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } else {
            repository.notConnected()
        }
    }
}