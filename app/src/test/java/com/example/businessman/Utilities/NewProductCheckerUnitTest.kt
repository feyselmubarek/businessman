package com.example.businessman.Utilities

import org.junit.Test

class NewProductCheckerUnitTest{
    @Test
    fun testProductPrice(){
        val incorrectPassword = -0.8
        val incorrectResult = NewProductChecker.checkProductPrice(incorrectPassword)
        val correctPassword = 9.0
        val correctResult = NewProductChecker.checkProductPrice(correctPassword)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductQuantity(){
        val correctUserName = 7
        val correctResult = NewProductChecker.checkProductQuantity(correctUserName)
        val incorrectUserName = -10
        val incorrectResult = NewProductChecker.checkProductQuantity(incorrectUserName)

        assert(!incorrectResult)
        assert(correctResult)
    }

    @Test
    fun testProductName(){
        val incorrectProductName = "908"
        val incorrectProductName2 = "````````````````````````````````````"

        val incorrectResult = NewProductChecker.checkProductName(incorrectProductName)
        val incorrectResult2 = NewProductChecker.checkProductName(incorrectProductName2)

        assert(!incorrectResult)
        assert(!incorrectResult2)
    }
}