package com.example.businessman.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class User(
    @PrimaryKey val id:Int = -1,
    @ColumnInfo val username: String?,
    @ColumnInfo val password:String?,
    @ColumnInfo val name:String
):Serializable{
    companion object {
        var loggedInUser: User? = null
    }
}