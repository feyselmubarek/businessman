package com.example.businessman.repository

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast
import androidx.lifecycle.LiveData
import com.example.businessman.data.BusinessManDao
import com.example.businessman.data.Product
import com.example.businessman.data.Transaction
import com.example.businessman.data.User
import com.example.businessman.network.BMAPIServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class BMRepository(val context : Context, private val bM_APIServices : BMAPIServices,
                   private val bm_dao : BusinessManDao) {
    /**
     * Syncing room and retrofit database
     */
    fun syncDatabase(id: Int) : LiveData<List<Product>>{
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Product>> = bM_APIServices.getMyProductAsync(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        bm_dao.insertProduct(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return bm_dao.getAllProducts()
    }

    /**
     * Syncing room and retrofit database
     */
    fun syncTraDatabase(id: Int) : LiveData<List<Transaction>>{
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Transaction>> = bM_APIServices.getMyTransactionAsync(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        bm_dao.insertTransaction(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return bm_dao.getAllTransaction()
    }

    fun getAllTransaction(id: Int) : LiveData<List<Transaction>>{
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Transaction>> = bM_APIServices.getMyTransactionAsync(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        bm_dao.insertTransaction(*it.toTypedArray())
                    }
                }
            }
        } else {
//            notConnected()
        }
        return bm_dao.getAllTransaction()
    }

    /**
    * getting all the products for logged in user
    * */
    fun getMyProducts(id: Int): LiveData<List<Product>> {
        if (isConnected()) {
            GlobalScope.async(Dispatchers.IO) {
                val response: Response<List<Product>> = bM_APIServices.getMyProductAsync(id).await()
                if (response.isSuccessful) {
                    response.body()?.let {
                        bm_dao.insertProduct(*it.toTypedArray())
                    }
                }
            }
        } else {
            notConnected()
        }
        return bm_dao.getAllProducts()
    }

    /**
    * getting product by id
    * */
    fun getProductById(id: Int): Product? {
        var product: Product? = null
        if (isConnected()) {
            runBlocking(Dispatchers.IO) {
                val response: Response<Product> = bM_APIServices.getProductAsync(id).await()
                if (response.isSuccessful) {
                    product = response.body()
                }
            }
        } else {
            notConnected()
        }
        return product
    }

    /**
     *
     */
    suspend fun updateProduct(product: Product) : Response<Product> {
        return  bM_APIServices.updateProductAsync(product).await()
    }

    /**
     *
     */
    suspend fun deleteProduct(productId:Int) : Response<Product>{
       return bM_APIServices.deleteProductAsync(productId).await()
    }

    /**
    * Adds user to database
    * */
    suspend fun addUser(user: User): Response<User> {
        return bM_APIServices.addUserAsync(user).await()
    }

    /**
    * Adds product to database
    * */
    suspend fun addProduct(product: Product): Response<Product> {
        return bM_APIServices.addProductAsync(product).await()
    }

    /**
     * Adds product to database
     * */
    suspend fun addTransaction(transaction: Transaction): Response<Transaction> {
        return bM_APIServices.addTransactionAsync(transaction).await()
    }

    /**
     * Uploads image file of product
     */
    suspend fun uploadProductImage(description: RequestBody, file: MultipartBody.Part)
            : Response<RequestBody>{
        return bM_APIServices.uploadProductImageAsync(description, file).await()
    }

    /**
     * Clearing room database
     * */
    fun clearDatabase() {
        GlobalScope.async {
            bm_dao.deleteAllProduct()
            bm_dao.deleteAllTransaction()
        }
    }

    /**
    * Makes a toast to notify connection failed
    */
    fun notConnected() {
        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
    }

    /**
    * Makes a toast to notify request failed
    */
    private fun requestFailed() {
        Toast.makeText(context, "Request Failed", Toast.LENGTH_SHORT).show()
    }

    /**
    * Helper Method
    * */
    fun showUser(user: User){
        Toast.makeText(context, user.username, Toast.LENGTH_SHORT).show()
    }

    /**
    * Check network connection arability
    */
    fun isConnected(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}