package com.example.businessman.Utilities

class TransactionChecker {
    companion object{
        fun checkProductPrice(price: Double): Boolean {
            return price > 0.0
        }

        fun checkProductQuantity(quantity: Int): Boolean {
            return quantity > 0
        }
    }
}