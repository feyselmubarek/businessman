package com.example.businessman.network

import com.example.businessman.data.Product
import com.example.businessman.data.Transaction
import com.example.businessman.data.User
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface BMAPIServices {

    @POST("auth/login")
    fun loginUserAsync(@Body user: User): Deferred<Response<User>>

    @POST("auth/register")
    fun addUserAsync(@Body user: User): Deferred<Response<User>>

    @POST("product/new")
    fun addProductAsync(@Body product: Product): Deferred<Response<Product>>

    @GET("product/user/{id}")
    fun getMyProductAsync(@Path("id") id: Int): Deferred<Response<List<Product>>>

    @GET("product/{id}")
    fun getProductAsync(@Path("id") id: Int): Deferred<Response<Product>>

    @PUT("product/update")
    fun updateProductAsync(@Body product: Product) : Deferred<Response<Product>>

    @DELETE("product/{id}")
    fun deleteProductAsync(@Path("id") id: Int) : Deferred<Response<Product>>

    @GET("transaction/user/{id}")
    fun getMyTransactionAsync(@Path("id") id: Int) : Deferred<Response<List<Transaction>>>

    @POST("transaction/new")
    fun addTransactionAsync(@Body transaction: Transaction) : Deferred<Response<Transaction>>

    @Multipart
    @POST("product/upload")
    fun uploadProductImageAsync(
        @Part("description") requestBody: RequestBody,
        @Part photo: MultipartBody.Part
    ) : Deferred<Response<RequestBody>>

    companion object {
        private const val baseUrl =  "http://10.6.250.131:5002/api/"

        fun getInstance(): BMAPIServices {

            val client = OkHttpClient
                .Builder()
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

            return retrofit.create(BMAPIServices::class.java)
        }
    }
}